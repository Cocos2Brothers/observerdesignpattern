#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

class Observer;

class Observable  
{
    virtual void registerObserver( Observer * obs ) = 0;
    virtual void notifyObserver() = 0;
    virtual void deregisterObserver( Observer * obs ) = 0;
};

class Observer
{
public:
    virtual void update(Observable *) = 0;
};



class GazetaPoranna : public Observable
{
public:
    std::vector <Observer *> obserwowane; 

    virtual void registerObserver( Observer * obs ) override
    {
        obserwowane.push_back( obs );

    }
    virtual void notifyObserver() override
    {
        auto it = obserwowane.begin();
        for ( ; it != obserwowane.end(); ++it )
        {
            ( *it )->update(this );
        }

    }
    virtual void deregisterObserver( Observer * obs ) override
    {
        std::vector <Observer *> &v = obserwowane;
        v.erase( std::remove (v.begin(), v.end(), obs ), v.end() );
    }
};



class Czytelnik : public Observer
{
    std::string nazwa; 
    Observable * observable;
public:
    Czytelnik( std::string naz, Observable * obs ): nazwa( naz ), observable(obs) {};

    virtual void update( Observable * obs ) override
    {
        if ( obs == observable )
        {
            std::cout << " jestem poinformowany " << nazwa << std::endl;
        }
    }
};

int main () 
{

    GazetaPoranna gazetka; 

    Czytelnik grzes ("grzes", &gazetka ),
        andrzejek ("andrzejek", &gazetka ),
        huj ( " czysty huj ", &gazetka );

    gazetka.registerObserver( &grzes );
    gazetka.registerObserver( &grzes );
    gazetka.registerObserver( &andrzejek );
    gazetka.registerObserver( &andrzejek );
    gazetka.registerObserver( &huj );
    gazetka.notifyObserver();

    std::cout << " usuniecie grzeska dodaniwe drugiego huja" << std::endl;
    gazetka.registerObserver( &huj );
    gazetka.deregisterObserver( &grzes );
    gazetka.notifyObserver();

    system( "pause" );
    return 0; 
}